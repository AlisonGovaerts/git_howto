---
title: "Git, GitLab & RStudio"
author: "Tom Cox"
date: '2018-12-04'
output:
  html_document: default
  html_notebook: default
---

## 1. Create Gitlab account and install git on your computer 


***1.1 GitLab account: on https://gitlab.com/ ***

In GitLab you can make a test project right away. Just click "New Project" and you're off to go (mind you, before you can work locally on this project, you have some more steps to take, see below).

The project master can add new developers to the project.

    Goto settings (bottom left column)
    Click "Members"

***1.2 Install git: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git ***

**In MS Windows** Don't forget to untick the ‘Git Credential Manager’ checkbox during the Git for Windows installer (otherwise, see below)
See: https://stackoverflow.com/questions/37182847/how-to-disable-git-credential-manager-for-windows

## 2. Check out your first repository from within RStudio
  
  If you want to add an existing project on your computer to git and GitLab, see below (7. Basic command line instructions). 
  
  To start working on an existing remote project on which you are granted to work as developer:
  
  In RStudio
    
    File -> New Project -> Version Control -> Git
    
  Repository URL: https://gitlab.com/[user]/[Project]
    
  Working directory: choose what you like (But not where you already have files you are working on. Move these first to a temporary location)
  
**Congratulations: you can now start working on a local copy of the remote repository **

If you want to make a personal copy of an existing project on which you don't have developer access, you have to fork it first to your own account, and start working from there. For example, to start working on this HowTo, go to

    Go to https://gitlab.com/tcox/git_howto
    Click fork

You can now go back to your own account, find the URL of this project and proceed like above.

If you want to work on a specific branch (not the master):  At the Git tab (top right in RStudio) click on downarrow next to Master
Choose remote -> branch_name


## 3. Alternative: checkout from the command line
In terminal, go to the directory where you want to have the scripts

    git clone https://gitlab.com/[user]/[project]

Congratulations: you can now start working on a local copy of the remote repository

If you want to work on a specific branch (not the master)

    git branch -a
    git checkout -b [branchname]

Congratulations: you are now working on a local copy of the roos_devel branch. 

Comment:
Changing local branch in Rstudio (by clicking on the arrow next to 'master' in the git tab) is the same as changing local branch in command line (more --> shell --> git checkout branch_name). In terminal you can see which branch you are working on with 'git branch' 

## 4. Normal workflow in RStudio
In RStudio, the normal git workflow goes like this

- work on your local files in RStudio as you would normally do without git
- regularly 'commit' the changes, so that git 'locally' starts to track changes
- 'push' the changes as much as you want (after committing). now your changes are also on-line, and viewable by other developers
- 'pull' the changes from the remote if other developers have changed some things

Once you've set up an project to use git (see 2.) you find 'commit', 'push' and 'pull' in the "git" tab at the topright in RStudio. Just click!

You can make changes to this HowTo, commit them to git, and push them to your own on-line repo (on your GitLab account)

If you made changes you believe should be in this HowTo, create a merge request: https://docs.gitlab.com/ee/workflow/forking_workflow.html

I'll review what you suggest, and either accept or decline the merge request (haha, I'm the master)

This is the basics. Check  https://about.gitlab.com/2014/09/29/gitlab-flow/ for more advanced workflows.

## 5. Some common git commands ##

    git clone https://gitlab.com/[user]/[project]
Copies all information from the remote repository to your local machine

    git branch 
Shows all branches in the local repository

    git branch -a 
Shows all local and remote branches

    git checkout -b [branchname]

creates lokal branch tracking remote branch with same name name
all commits will be done to this branch, all pushes will be done to remote branch with the same name

    git status
Check which files in the local folder are tracked by git (locally)


## 6. Advanced git ##

**deleting a branch **

    git branch -d <name of branch>
Delete a local branch
See: https://makandracards.com/makandra/621-git-delete-a-branch-local-or-remote

**merging and rebasing **

see: https://www.atlassian.com/git/tutorials/merging-vs-rebasing
it seems rebasing is what we need most.

whan master has been updated in the mean time, you can merge the new master into a different branch

    git checkout [branchname] 
    git merge master 

line 1 means: start working on the 'local' development branch - see above when this does not yet exist)
line 2 means: merge changes in master to roos_devel

see: 
https://stackoverflow.com/questions/16955980/git-merge-master-into-feature-branch?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

alternative approach:
    
    git checkout [branchname] 
    git pull --rebase origin master 

see:
https://stackoverflow.com/questions/7929369/how-to-rebase-local-branch-with-remote-master?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa


**Switching between versions, bringing back deleted files **
S
ee: 
http://blog.kablamo.org/2013/12/08/git-restore/


    git log : gives all commits, with commit hashes
    git log 
    git log --diff-filter=D --summary   # shows all deleted files ever
    git log --diff-filter=D --author=Batman --summary  # all files deleted by Batman

    git checkout <commit hash> . # checks out all files of a former commit
    git checkout <commit hash> /path/to/file # checks out older file

**To revert back to head without committing**
    git checkout -f     # the -f forces the working tree to be cleaned, so if deleted files where brought back by going to an older versions, they will be deleted again

to re-add a deleted file, just commit them


## 7. Basic command line instructions, copied from Gitlab ##
 
Git global setup:

    git config --global user.name "name"
    git config --global user.email "e-mail"

Create a new repository

    git clone https://gitlab.com/tcox/tmp.git
    cd tmp
    touch README.md
    git add README.md
    git commit -m "add README"
    git push -u origin master

Existing folder

    cd existing_folder
    git init
    git remote add origin https://gitlab.com/tcox/tmp.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master

Existing Git repository

    cd existing_repo
    git remote rename origin old-origin
    git remote add origin https://gitlab.com/tcox/tmp.git
    git push -u origin --all
    git push -u origin --tags

More cammand line instructions: https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html

## 8. Manually disabling credential manager:

When you forgot to disable the credential manager during installation, proceed as follows. 

  Check in the config file if credentials are remembered (should be off, so that you will be asked every time)
  
    RStudio  open shell: git config --edit –system 
    
  
```{r Git_Config_Output, echo=FALSE, out.width = '75%'}
knitr::include_graphics("./screenshots/Git_Config_Output.jpg")
```
  
  The line ‘helper = manager’ should not be there.
  If the line ‘helper = manager’ is there, you will probably need to adjust the writing permissions in order to change it. In windows explorer,    go to the location where your ‘etc’ file is stored. For me, this was default:

    C:\Program Files\Git\mingw64\etc
  Here you should find the ‘gitconfig’ file.
  Right click to 
  
    Eigenschappen>Beveiliging>Bewerken -> ‘Volledig beheer’ (this includes writing) to be ticked for: ‘Alle toepassingspakketten’, ‘Alle toepassingspakketten met beperkingen’, ‘System’, ‘(eigen user)’, ‘Administrators’ en ‘Gebruikers’. 

  (I kept this in Dutch to make sure I put the exact words. If your system language is set in English, it should be clear from the screenshots   below. Probably something like ‘properties>Security…)


```{r Gitconfig_permissions, echo=FALSE, out.width = '75%'}
knitr::include_graphics("./screenshots/Gitconfig_permissions.png")
```
  Go back to the Shell and launch the commands:
    git config --system --unset credential.helper
    git config --edit –system 
     check if the ‘helper = manager’ line has been deleted.



 

